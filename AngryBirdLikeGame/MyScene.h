//
//  MyScene.h
//  AngryBirdLikeGame
//

//  Copyright (c) 2014年 kitano. All rights reserved.
//

#import <SpriteKit/SpriteKit.h>

@interface MyScene : SKScene <SKPhysicsContactDelegate, UIGestureRecognizerDelegate>

-(float)calculaAngulo:(CGPoint) ponto1 ponto2:(CGPoint) ponto2;

@end
