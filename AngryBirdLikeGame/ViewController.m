//
//  ViewController.m
//  AngryBirdLikeGame
//
//  Created by kitano on 2014/01/17.
//  Copyright (c) 2014年 kitano. All rights reserved.
//

#import "ViewController.h"
#import "MyScene.h"

@interface ViewController ()

-(void)reset;

@end

@implementation ViewController
@synthesize skView;

- (void)viewDidLoad
{
    [super viewDidLoad];

}

- (void)viewWillLayoutSubviews
{
    [super viewWillLayoutSubviews];
        // Configure the view.
    self.skView = (SKView *)self.view;
    if (skView.scene == nil) {
        skView.showsFPS = YES;
        skView.showsNodeCount = YES;
        
        // Create and configure the scene.
        SKScene * scene = [MyScene sceneWithSize:skView.bounds.size];
        NSLog(@"%@",NSStringFromCGSize(skView.bounds.size));
        scene.scaleMode = SKSceneScaleModeAspectFill;
        
        // Present the scene.
        [skView presentScene:scene];
    }
    
    UIButton *reset = [[UIButton alloc] initWithFrame:CGRectMake(10, 100, 30, 30)];
    reset.backgroundColor = [UIColor greenColor];
    [reset addTarget:self action:@selector(reset) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:reset];
}

-(void)reset
{
    NSLog(@"The zoera never ends");
    SKScene * scene = [MyScene sceneWithSize:skView.bounds.size];
    NSLog(@"%@",NSStringFromCGSize(skView.bounds.size));
    scene.scaleMode = SKSceneScaleModeAspectFill;
    [skView presentScene:scene transition:[SKTransition flipHorizontalWithDuration:2]];
}
- (BOOL)shouldAutorotate
{
    return YES;
}

- (NSUInteger)supportedInterfaceOrientations
{
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
        return UIInterfaceOrientationMaskAllButUpsideDown;
    } else {
        return UIInterfaceOrientationMaskAll;
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Release any cached data, images, etc that aren't in use.
}

@end
