//
//  MyScene.m
//  AngryBirdLikeGame
//
//  Created by kitano on 2014/01/17.
//  Copyright (c) 2014年 kitano. All rights reserved.
//

#import "MyScene.h"
#import <SpriteKit/SpriteKit.h>
#import "ViewController.h"

@interface MyScene ()

@property (nonatomic) double currentTime;
@property (nonatomic) double startTime;
@property (nonatomic) double endTime;

@property (nonatomic) CGPoint pontoInicial;
@end

enum
{
    kDragNone,
    kDragStart,
    kDragEnd,
};

@implementation MyScene
{
    SKSpriteNode *ball;
    SKSpriteNode *target;
    SKSpriteNode *celaFixo;
    SKSpriteNode *celaBraco;
    int  gameStatus;
    CGPoint startPos;
    
}

-(id)initWithSize:(CGSize)size {    
    if (self = [super initWithSize:size]) {
        /* Setup your scene here */
        
        self.backgroundColor = [SKColor colorWithRed:0.15 green:0.15 blue:0.3 alpha:1.0];
        self.anchorPoint = CGPointMake(0.5f, 0.5f);
        gameStatus = kDragNone;
        
        SKNode *myWorld = [SKNode node];
        myWorld.name = @"world";
        [self addChild:myWorld];
        
        SKNode *camera = [SKNode node];
        camera.name = @"camera";
        [myWorld addChild:camera];

        SKSpriteNode *ground = [[SKSpriteNode alloc] initWithColor:[SKColor brownColor]
                                                              size:CGSizeMake(size.width*10000,
                                                                              size.height)];
        ground.position    = CGPointMake(0, -ground.size.height + 30);
        ground.physicsBody = [SKPhysicsBody bodyWithRectangleOfSize:ground.size];
        ground.physicsBody.dynamic = NO;
        [myWorld addChild:ground];
        
        
        celaFixo = [SKSpriteNode spriteNodeWithImageNamed:@"celarino_body.png"];
        celaFixo.position = CGPointMake(100-self.size.width/3, -(self.size.height/2)+120);
        celaFixo.size = CGSizeMake(65, 230);
        [myWorld addChild:celaFixo];
        
        celaBraco = [SKSpriteNode spriteNodeWithImageNamed:@"celarino_arm.png"];
        celaBraco.position = CGPointMake(85-self.size.width/3, -(self.size.height/2)+175);
        celaBraco.size = CGSizeMake(35, 90);
        celaBraco.anchorPoint = CGPointMake(0.42, 0.85); //0.42 0.85
        [myWorld addChild:celaBraco];
        //SKAction *rotation = [SKAction rotateByAngle: M_PI/4.0 duration:0];
        //and just run the action
        //[celaBraco runAction: rotation];
        
        
        ball = [SKSpriteNode spriteNodeWithImageNamed:@"ball.png"];
        ball.position = CGPointMake(105-self.size.width/3, -(self.size.height/2)+95);
        ball.name = @"ball";
        ball.physicsBody = [SKPhysicsBody bodyWithCircleOfRadius:ball.size.width/2];
        ball.physicsBody.dynamic = NO;
        [myWorld addChild:ball];
        
        
//        UIPanGestureRecognizer *longPanGesture = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(panRecognized:)];
//        longPanGesture.minimumNumberOfTouches = 1;
//        longPanGesture.maximumNumberOfTouches = 1;
//        [self.view addGestureRecognizer:longPanGesture];
//        
        //int start_x = 600;
//        target = [SKSpriteNode spriteNodeWithColor:[SKColor redColor] size:CGSizeMake(15,15)];
//        target.physicsBody = [SKPhysicsBody bodyWithRectangleOfSize:target.size];
//        target.physicsBody.contactTestBitMask = 0x1 << 0;
//        target.position = CGPointMake(start_x + 150,-35);
//        [myWorld addChild:target];

        
        self.physicsWorld.contactDelegate = self;
    }
    return self;
}

-(float)calculaAngulo:(CGPoint) ponto1 ponto2:(CGPoint) ponto2
{
    float angulo = 0;
    
    angulo = atan2(ponto2.y - ponto1.y, ponto2.x - ponto1.x);
    
    if (angulo < 0)
    {
        angulo = 2 * M_PI + angulo;
    }
    return angulo;
}


//-(void)panRecognized:(id)sender
//{
//    if ([sender state] == UIGestureRecognizerStateBegan)
//    {
//        //move o braco
//        
//        [self.calculaAngulo:];
//        
//        
//        
//        SKAction *rotation = [SKAction rotateByAngle:  duration:0];
//        //and just run the action
//        [celaBraco runAction: rotation];
//        
//    }
//    else if ([sender state] == UIGestureRecognizerStateEnded)
//    {
//        SKAction *rotation = [SKAction rotateByAngle: M_PI/8.0 duration:0];
//        //and just run the action
//        [celaBraco runAction: rotation];
//    }
//    else if ([sender state] == UIGestureRecognizerStateChanged)
//    {
//        SKAction *rotation = [SKAction rotateByAngle: M_PI/8.0 duration:0];
//        //and just run the action
//        [celaBraco runAction: rotation];
//    }
//}

- (void)didBeginContact:(SKPhysicsContact *)contact
{
    if (ball.position.y <= 0)
    {
        ball.speed = 0;

    }
    
        NSString *path = [[NSBundle mainBundle] pathForResource:@"Spark" ofType:@"sks"];
        SKEmitterNode *spark = [NSKeyedUnarchiver unarchiveObjectWithFile:path];
        spark.numParticlesToEmit = 50;
        spark.particlePosition = contact.contactPoint;
        [self addChild:spark];
        [target removeFromParent];
}

- (void)didSimulatePhysics
{
    SKNode *camera = [self childNodeWithName: @"//camera"];
    if(gameStatus == kDragEnd && ball.position.x > 0)
        camera.position = CGPointMake(ball.position.x, camera.position.y);
    [self centerOnNode: camera];
}

- (void) centerOnNode: (SKNode *) node
{
    CGPoint cameraPositionInScene = [node.scene convertPoint:node.position fromNode:node.parent];
    node.parent.position = CGPointMake(node.parent.position.x - cameraPositionInScene.x, node.parent.position.y - cameraPositionInScene.y);
}


-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    
    for (UITouch *touch in touches)
    {
        CGPoint location = [touch locationInNode:self];
        
        self.pontoInicial = location;
        
        SKNode *node = [self nodeAtPoint:location];
        gameStatus = kDragStart;
        startPos = location;
        self.startTime = self.currentTime;
    }
}



- (void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event {
    if(gameStatus == kDragStart ){
        UITouch *touch = [touches anyObject];
        CGPoint touchPos = [touch locationInNode:self];
        ball.position = touchPos;
    }
    
    UITouch *touch = [touches anyObject];
    CGPoint location = [touch locationInNode:self];
    
    float angulo = [self calculaAngulo:self.pontoInicial ponto2:location];
    
    SKAction *rotation = [SKAction rotateByAngle: angulo duration:0];
    //and just run the action
    [celaBraco runAction: rotation];
    
}

- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event {
    if(gameStatus == kDragStart ){
        gameStatus = kDragEnd;

        UITouch *touch = [touches anyObject];
        CGPoint endPos = [touch locationInNode:self];

        CGPoint diff = CGPointMake(endPos.x - startPos.x, endPos.y - startPos.y);
        self.endTime = self.currentTime;
        
        double diffTime = self.endTime - self.startTime;
        NSLog(@"%f", diffTime);
        ball.physicsBody.dynamic = YES;
        [ball.physicsBody applyForce:CGVectorMake((diff.x * 20/diffTime), (diff.y * 50/diffTime))];

        SKAction *scaleOut = [SKAction scaleTo:0.5 duration:0.2];
        SKAction *moveUp   = [SKAction moveByX:0   y:-100 duration:0.2];
        SKAction *scale1   = [SKAction group:@[scaleOut,moveUp]];
        SKAction *delay    = [SKAction waitForDuration:1.0];
        SKAction *scaleIn  = [SKAction scaleTo:1 duration:1.0];
        SKAction *moveDown = [SKAction moveByX:0   y:100 duration:1.0];
        SKAction *scale2   = [SKAction group:@[scaleIn,moveDown]];
        SKAction *moveSequence = [SKAction sequence:@[scale1, delay,scale2]];
        [self runAction:moveSequence];

        
    }
    
    UITouch *touch = [touches anyObject];
    CGPoint location = [touch locationInNode:self];
    
    float angulo = [self calculaAngulo:self.pontoInicial ponto2:location];
    
    SKAction *rotation = [SKAction rotateByAngle: angulo duration:0];
    //and just run the action
    //[celaBraco runAction: rotation];
    
    
    
}


-(void)update:(CFTimeInterval)currentTime {
    /* Called before each frame is rendered */
    self.currentTime = currentTime;
    //NSLog(@"%f", currentTime);
    SKNode *camera = [self childNodeWithName: @"//camera"];
    if(gameStatus == kDragEnd && ball.position.x > 0)
        camera.position = CGPointMake(ball.position.x, camera.position.y);
    [self centerOnNode: camera];

}

@end
